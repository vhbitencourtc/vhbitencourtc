<h1 align="center">Hello, I'm Victor Hugo Bitencourt Coutinho🚀</h1>
<h3 align="center">🔭I’m currently working on Cencosud S.A. as a Full Systems Technician.</h3>

<p align="left"><img src="https://komarev.com/ghpvc/?username=vhbitencourtc&color=blue" alt="vhbitencourt"/>✨</p>

<h2 align="left">🌱I’m currently learning  HTML, CSS, JavaScrip e Node.JS .🔍️</h2>
<p align="left"><a href="https://getbootstrap.com/docs/5.0/getting-started/introduction/" target="_blank"><img src="https://logos-download.com/wp-content/uploads/2017/07/HTML5_badge.png" alt="html5" widht="50" height="50"/></a><a href="https://getbootstrap.com/docs/5.0/getting-started/introduction/" target="_blank"><img src="https://maxcdn.icons8.com/Share/icon/Logos/css31600.png" alt="css3" widht="50" height="50" /></a><a href="https://getbootstrap.com/docs/5.0/getting-started/introduction/" target="_blank"><img src="https://clipartart.com/images/javascript-icon-clipart-6.png" alt="javascript" widht="50" height="50"/></a> <a href="https://getbootstrap.com/docs/5.0/getting-started/introduction/" target="_blank"><img src="https://th.bing.com/th/id/R.f5d8e6aecfb22f80bfcc92dc54e056c9?rik=rmMBZDa%2fbRIM9w&pid=ImgRaw" alt="node.js" widht="50" height="50"/></a></p>

<h2 align="left"><strong>📱Follow me the good ones:</strong></h2>
<p align="left"><a href="https://www.linkedin.com/in/vhbitencourtc/" target="_blank"> <img src="https://image.flaticon.com/icons/png/512/174/174857.png" alt="linkedin" widht="50" height="50"/></a> <a href="https://www.instagram.com/vhbitencourtc/" target="_blank"> <img src="https://th.bing.com/th/id/R.26d9974a1feec9905a4e0d5e5ddf8db6?rik=ycoXFwG5Udz08A&pid=ImgRaw" alt="instagram" widht="50" height="50"/></a> <a href="https://twitter.com/vhbitencourtc" target="_blank"> <img src="https://imagepng.org/wp-content/uploads/2018/08/twitter-icone.png" alt="twitter" widht="50" height="50"/></a> <a href="https://www.facebook.com/vhbitencourtc" target="_blank"><img src="https://1.bp.blogspot.com/-E7Q8QGQi8jU/WImcvZPvYQI/AAAAAAAACTw/0Er2C5lpPrkRx_JMFTMU0ifRdjS3e4XJQCLcB/s1600/VEKTOR%2BICON7.png" alt="facebook" widht="50" height="50"/></a></p>


  <h2 align="down">👽️💚Do you consider yourself Nerd or Geek? If so, follow us here too:📱</h2>
  <p align="down"><a href="https://www.instagram.com/lojageekofthrones/" target="_blank"> <img src="https://th.bing.com/th/id/R.20a13c58563faeabdacb022214eebcc1?rik=xuPcsREskL8LAw&pid=ImgRaw" alt="instageekofthrones" widht="50" height="50"/></a><a href="https://www.facebook.com/lojageekofthrones" target="_blank"> <img src="https://th.bing.com/th/id/R.70da29e0cd3e98f2ba4bb67bd0bde726?rik=CoFXoBDkiNbY5w&pid=ImgRaw" alt="facegeekofthrones" widht="50" height="50"/></a><a href="https://twitter.com/ljgeekofthrones" target="_blank"> <img src="https://th.bing.com/th/id/R.378f8d0e6a1f813408cb197f76ff5905?rik=qFCqXx9pIV0Olg&riu=http%3a%2f%2ficons.iconarchive.com%2ficons%2fampeross%2fsmooth%2f512%2fTwitter-icon.png&ehk=6RpELAirt9UnFZ7kuRmyEh28PMVhgijsMo83oJW39ws%3d&risl=&pid=ImgRaw" alt="twittergeekofthrones" widht="50" height="50"/></a></p>

<!--
**vhbitencourtc/vhbitencourtc** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
